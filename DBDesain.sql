/*
Payment
Checkout
https://bmeviauac01.github.io/datadriven-en/db/
https://dba.stackexchange.com/questions/290644/database-design-for-one-payment-and-monthly-payments
*/


DROP DATABASE IF EXISTS proyekakhirtim1;

CREATE DATABASE proyekakhirtim1;

CREATE TABLE proyekakhirtim1.tbl_type_product(
pk_id_type INT(11) PRIMARY KEY AUTO_INCREMENT,
type_name VARCHAR(25) NOT NULL,
SOURCE VARCHAR(100) NOT NULL
);

CREATE TABLE proyekakhirtim1.tbl_type_user(
pk_user_type_id INT(11) PRIMARY KEY AUTO_INCREMENT,
user_type_name VARCHAR(25) NOT NULL
);

CREATE TABLE proyekakhirtim1.tbl_users(
pk_user_id INT(25) PRIMARY KEY AUTO_INCREMENT,
NAME VARCHAR(40) NOT NULL,
email VARCHAR(25) NOT NULL UNIQUE,
PASSWORD VARCHAR(100) NOT NULL,
fk_user_type_id INT(11) NOT NULL,
is_active TINYINT(1) NOT NULL DEFAULT 0,
is_login TINYINT(1) NOT NULL DEFAULT 0,
CONSTRAINT fk_user_type_id FOREIGN KEY(fk_user_type_id) REFERENCES proyekakhirtim1.tbl_type_user(pk_user_type_id),
CONSTRAINT check_is_active CHECK (is_active>=0 AND is_active<=1),
CONSTRAINT check_is_login CHECK (is_login>=0 AND is_login<=1)
);

CREATE TABLE proyekakhirtim1.tbl_courses(
pk_id_course INT(20) PRIMARY KEY AUTO_INCREMENT,
course_name VARCHAR(25) NOT NULL,
DESCRIPTION VARCHAR(65535),
SOURCE VARCHAR(100) NOT NULL,
price_course VARCHAR(25) NOT NULL,
fk_id_type INT(11),
CONSTRAINT fk_id_type
    FOREIGN KEY (fk_id_type) REFERENCES proyekakhirtim1.tbl_type_product(pk_id_type)
);

CREATE TABLE proyekakhirtim1.tbl_schedules(
pk_id_schedule INT(20) PRIMARY KEY AUTO_INCREMENT,
date_schedule DATE,
is_active TINYINT(1) DEFAULT 0,
CONSTRAINT check_schedules_is_login CHECK (is_active<=1 AND is_active>=0)
);

CREATE TABLE proyekakhirtim1.tbl_course_schedule(
pk_id_tbl_course_schedule INT(20) PRIMARY KEY AUTO_INCREMENT,
fk_id_schedule INT(20),
fk_id_course INT(20),
CONSTRAINT fk_id_schedule
    FOREIGN KEY (fk_id_schedule) REFERENCES proyekakhirtim1.tbl_schedules(pk_id_schedule),
CONSTRAINT fk_id_course
    FOREIGN KEY (fk_id_course) REFERENCES proyekakhirtim1.tbl_courses(pk_id_course),
UNIQUE(fk_id_schedule,fk_id_course)
);

CREATE TABLE proyekakhirtim1.tbl_type_invoices(
pk_id_type_invoices INT (25) PRIMARY KEY AUTO_INCREMENT,
type_invoices_name VARCHAR(20)
);

CREATE TABLE proyekakhirtim1.tbl_invoices(
pk_id_invoice INT(25) PRIMARY KEY AUTO_INCREMENT,
date_payment DATE NOT NULL,
total_price INT(100) NOT NULL,
fk_id_user INT(25) NOT NULL,
CONSTRAINT  fk_id_user_1 FOREIGN KEY (fk_id_user) REFERENCES proyekakhirtim1.tbl_users(pk_user_id)
);

CREATE TABLE proyekakhirtim1.tbl_course_invoice(
pk_id_course_invoice INT(25) PRIMARY KEY AUTO_INCREMENT,
fk_id_course INT(25),
fk_id_invoice INT(25),
CONSTRAINT fk_id_course_1 FOREIGN KEY (fk_id_course) REFERENCES proyekakhirtim1.tbl_courses(pk_id_course),
CONSTRAINT fk_id_invoice_1 FOREIGN KEY (fk_id_invoice) REFERENCES proyekakhirtim1.tbl_invoices(pk_id_invoice),
CONSTRAINT unique_course_invoice UNIQUE (fk_id_course,fk_id_invoice)
);


INSERT INTO proyekakhirtim1.tbl_type_product
VALUES  (DEFAULT,'Electric','http://localhost:3000/Image/TypeCard/typeCard-electric.png'), #1
	(DEFAULT,'Hatchback','http://localhost:3000/Image/TypeCard/typeCard-hatchback.png'), #2
	(DEFAULT,'LGC','http://localhost:3000/Image/TypeCard/typeCard-lcgc.png'), #3
	(DEFAULT,'MPV','http://localhost:3000/Image/TypeCard/typeCard-mpv.png'), #4
	(DEFAULT,'Offroad','http://localhost:3000/Image/TypeCard/typeCard-offroad.png'), #5
	(DEFAULT,'Sedan','http://localhost:3000/Image/TypeCard/typeCard-sedan.png'), #6
	(DEFAULT,'SUV','http://localhost:3000/Image/TypeCard/typeCard-suv.png'), #7
	(DEFAULT,'Truck','http://localhost:3000/Image/TypeCard/typeCard-truck.png'); #8

INSERT INTO proyekakhirtim1.tbl_schedules
VALUES 	(DEFAULT,'2022-07-25',DEFAULT),
	(DEFAULT,'2022-07-26',DEFAULT),
	(DEFAULT,'2022-07-27',DEFAULT),
	(DEFAULT,'2022-07-28',DEFAULT),
	(DEFAULT,'2022-07-29',DEFAULT),
	(DEFAULT,'2022-07-30',DEFAULT);
	
INSERT INTO proyekakhirtim1.tbl_courses
VALUES 
(DEFAULT,'Course SUV Kijang Innova',
'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci incidunt obcaecati eligendi exercitationem sed repudiandae distinctio quam voluptas fugiat, maxime aliquam totam quasi, voluptatibus est in eaque veniam soluta libero?',
'http://localhost:3000/Image/Course/Course_SUV_Kijang_Innova.png',
'IDR 700.000',7), #1
(DEFAULT,'Course LCGC Honda Brio',
'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci incidunt obcaecati eligendi exercitationem sed repudiandae distinctio quam voluptas fugiat, maxime aliquam totam quasi, voluptatibus est in eaque veniam soluta libero?',
'http://localhost:3000/Image/Course/Course_LCGC_Honda_Brio.png',
'IDR 500.000',3),#2
(DEFAULT,'Hyundai Palisade 2021',
'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci incidunt obcaecati eligendi exercitationem sed repudiandae distinctio quam voluptas fugiat, maxime aliquam totam quasi, voluptatibus est in eaque veniam soluta libero?',
'http://localhost:3000/Image/Course/Course_SUV_Hyundai_Palisade_2021.png',
'IDR 800.000',7),#3
(DEFAULT,'Course Mitsubishi Pajero',
'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci incidunt obcaecati eligendi exercitationem sed repudiandae distinctio quam voluptas fugiat, maxime aliquam totam quasi, voluptatibus est in eaque veniam soluta libero?',
'http://localhost:3000/Image/Course/Course_SUV_Mitsubishi_Pajero.png',
'IDR 800.000',7),#4
(DEFAULT,'Dump Truck for Mining Constructor',
'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci incidunt obcaecati eligendi exercitationem sed repudiandae distinctio quam voluptas fugiat, maxime aliquam totam quasi, voluptatibus est in eaque veniam soluta libero?',
'http://localhost:3000/Image/Course/Dump_Truck_for_Mining_Constructor.png',
'IDR 1.200.000',8),#5
(DEFAULT,'Sedan Honda Civic',
'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci incidunt obcaecati eligendi exercitationem sed repudiandae distinctio quam voluptas fugiat, maxime aliquam totam quasi, voluptatibus est in eaque veniam soluta libero?',
'http://localhost:3000/Image/Course/Course_Sedan_Honda_Civic.png',
'IDR 400.000',6);#6

INSERT INTO proyekakhirtim1.tbl_course_schedule
VALUES 	(DEFAULT,1,1),
	(DEFAULT,2,1),
	(DEFAULT,3,1),
	(DEFAULT,4,1),
	(DEFAULT,5,1),
	(DEFAULT,6,1),
	(DEFAULT,1,2),
	(DEFAULT,2,2),
	(DEFAULT,3,2),
	(DEFAULT,4,2),
	(DEFAULT,5,2),
	(DEFAULT,6,2),
	(DEFAULT,1,3),
	(DEFAULT,2,3),
	(DEFAULT,3,3),
	(DEFAULT,4,3),
	(DEFAULT,5,3),
	(DEFAULT,6,3),
	(DEFAULT,1,4),
	(DEFAULT,2,4),
	(DEFAULT,3,4),
	(DEFAULT,4,4),
	(DEFAULT,5,4),
	(DEFAULT,6,4),
	(DEFAULT,1,5),
	(DEFAULT,2,5),
	(DEFAULT,3,5),
	(DEFAULT,4,5),
	(DEFAULT,5,5),
	(DEFAULT,6,5),
	(DEFAULT,1,6),
	(DEFAULT,2,6),
	(DEFAULT,3,6),
	(DEFAULT,4,6),
	(DEFAULT,5,6),
	(DEFAULT,6,6);
	
INSERT INTO proyekakhirtim1.tbl_type_user
VALUES 	(DEFAULT,'user'),
	(DEFAULT,'admin');

