﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using TugasAkhirTim1.Models;
using TugasAkhirTim1.Services.EmailService;

namespace TugasAkhirTim1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        #region Configuration
        private readonly IConfiguration _configuration;
        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        #endregion
        #region
        [HttpGet]
        [Route("Logout")]
        [Authorize]
        public IActionResult Logout()
        {
            var re = Request;
            string token = re.Headers.Authorization[0].Replace("Bearer ", "");
            var TokenHandler = new JwtSecurityTokenHandler();
            var jwt = TokenHandler.ReadJwtToken(token);
            string id = "";
            id = jwt.Claims.First(c => string.Equals(c.Type, "ID")).Value;
            List<bool> loginActiveStatus = GetActiveLoginUser(id);
            if (loginActiveStatus[0]&& loginActiveStatus[1]) 
            {

                    SetIsLogin(0, id);
                    return Ok("Logout");

            }
           
            return BadRequest(id);

        }
        #endregion
        #region Verifikasi User
        [HttpPost]
        [Route("VerifyUser")]
        public IActionResult VerifyUser([FromBody] LoginModel model)
        {
            try
            {
                string query = "SELECT pk_user_id,password,is_active FROM tbl_users WHERE email=@email";
                string password; bool isActive; string id; string jwt;
                using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    MySqlDataAdapter dtAdapter = new MySqlDataAdapter(cmd);
                    cmd.Parameters.AddWithValue("@email", model.Email);
                    DataTable dt = new DataTable();
                    dtAdapter.Fill(dt);
                    DataRow dr = dt.Rows[0];
                    password = dr["password"].ToString();
                    isActive = Convert.ToBoolean(dr["is_active"]);
                    id = dr["pk_user_id"].ToString();
                    jwt = GenerateToken(id);
                    conn.Close();
                }
                if (isActive)
                    {
                        if (VerifyPassword(password, model.Password.ToString()) )
                        {
                        SetIsLogin(1, id);
                        return Ok(jwt);

                        }
                    
                    }
            return Unauthorized("Anda Salah Memasukan Password");
            }catch(Exception ex)
            {
                return Unauthorized("User Anda Tidak Terdaftar");
            }

        }
        #endregion

        #region CheckUserEmail
        private bool CheckUserEmail(string email)
        {
            string query = "SELECT email FROM tbl_users WHERE email=@email";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();

                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@email", email);
                    var getValue = cmd.ExecuteScalar();
                    if (getValue == null)
                    {
                        conn.Close();
                        return true;
                    }
                    conn.Close();
                    return false;


                }

            }
            catch (Exception ex)
            {

                return false;
            }


        }
        #endregion

        #region UserSession
        [HttpGet]
        [Route("UserSession")]
        public IActionResult UserSession()
        {

            var re = Request;
            string token = re.Headers.Authorization[0].Replace("Bearer ", "");
            var TokenHandler = new JwtSecurityTokenHandler();
            var jwt = TokenHandler.ReadJwtToken(token);
            string id = "";
            id = jwt.Claims.First(c => string.Equals(c.Type, "ID")).Value;
            List<bool> loginActiveStatus = GetActiveLoginUser(id);
            if (loginActiveStatus[0] && loginActiveStatus[1])
            {


                if (ValidateToken(token))
                {
                    return Ok(token);
                }
                else
                {
                    SetIsLogin(0, id);
                    return Unauthorized("Token Not Valid");
                }
            }
            return Unauthorized("Token Not Valid");
        }
        
        #endregion
        private bool VerifyPassword(string HashPass, string InputPass)
        {
            bool verified = BCrypt.Net.BCrypt.Verify(InputPass, HashPass);
            if (verified)
            {
                return true;
            }
            return false;
        }


        private string GenerateToken(string id)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim> { new Claim("ID", id) };

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);

            string jwt_User = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt_User;

        }
        private TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidIssuer = _configuration["Jwt:Issuer"],
                ValidAudience = _configuration["Jwt:Audience"],
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
            };
        }
        private bool ValidateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameter = GetValidationParameters();

            SecurityToken validatedToken;
            try
            {
                IPrincipal principal = tokenHandler.ValidateToken(token, validationParameter, out validatedToken);
                if (principal.Identity.IsAuthenticated)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        
        private void SetIsLogin(int isLogin,string id)
        {
            using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string querySetLogin = "UPDATE tbl_users SET is_login=@is_login WHERE pk_user_id=@id";
                MySqlCommand cmd = new MySqlCommand(querySetLogin, conn);
                cmd.Parameters.AddWithValue("@is_login", isLogin);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private List<bool> GetActiveLoginUser(string Id)
        {
                using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    string query = "SELECT is_active,is_login FROM tbl_users WHERE pk_user_id=@id";
                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@id", Id);
                    MySqlDataAdapter dtAdapter = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    dtAdapter.Fill(dt);
                    DataRow dr = dt.Rows[0];
                    bool isActive = Convert.ToBoolean(dr["is_active"]);
                    bool isLogin = Convert.ToBoolean(dr["is_login"]);
                    List<bool> ints = new List<bool>()
                    {
                        isActive,
                        isLogin
                    };
                    conn.Close();
                return ints;
                }

             
        }
    }


}

