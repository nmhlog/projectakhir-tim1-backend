﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TugasAkhirTim1.Models;
using TugasAkhirTim1.Services.EmailService;

namespace TugasAkhirTim1.Controllers.JwtToken
{
    public class JwtTokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public JwtTokenController(IConfiguration configuration)
        {
            _configuration = configuration;

        }

        protected virtual string GenerateToken(RegisterUserModel User)
        {
            string jwt_User = "";

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] { new Claim(ClaimTypes.Email, User.Email) };

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddHours(1),
                signingCredentials: credentials);

            jwt_User = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt_User;

        }

    }
}
