﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Data;
using System.Reflection.PortableExecutable;
using System.Xml.Linq;
using TugasAkhirTim1.Models;

namespace TugasAkhirTim1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypeProductController : ControllerBase
    {
        #region Configuration
        private readonly IConfiguration _configuration;
        public TypeProductController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        #endregion

        #region RoutesGetTypeProduct
        [HttpGet]
        [Route("GetTypeProduct")]
        public IActionResult GetTypeProduct(string? typeName) { 
            try
            {
                List<TypeProductModel> output = new List<TypeProductModel>();
                string query;
                if (String.IsNullOrEmpty(typeName))
                {
                    query = "SELECT pk_id_type,type_name,source FROM tbl_type_product";
                }
                else
                {
                    query = "SELECT pk_id_type,type_name,source FROM type_product WHERE tbl_type_product= @typeName";
                }
                using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@typeName", typeName);
                    MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);  
                    DataTable dtTypeProduct = new DataTable();
                    adapter.Fill(dtTypeProduct);
                    foreach(DataRow dr in dtTypeProduct.Rows)
                    {
                        output.Add(new TypeProductModel()
                        {
                            id= Convert.ToInt16(dr["pk_id_type"].ToString()),
                            type_name = dr["type_name"].ToString(),
                            source= dr["source"].ToString()
                        });
                    }
                    conn.Close();
                }
                return Ok(output);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        #endregion

        [HttpGet]
        [Route("GetCar")]
        public IActionResult GetCar()
        {
            List<ListCarModel> listCar = new List<ListCarModel>();
            string query = "SELECT tbl_courses.pk_id_course,tbl_courses.course_name,tbl_courses.Source,tbl_courses.price_course,tbl_type_product.type_name FROM tbl_courses INNER JOIN tbl_type_product ON tbl_type_product.pk_id_type = tbl_courses.fk_id_type LIMIT 6";
            try
            {
                using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    MySqlDataAdapter dataAdapter = new MySqlDataAdapter(cmd);
                    DataTable dtCar = new DataTable();
                    dataAdapter.Fill(dtCar);

                    foreach (DataRow dr in dtCar.Rows)
                    {
                        listCar.Add(new ListCarModel
                        {
                            Id = Convert.ToInt32(dr["pk_id_course"].ToString()),
                            Name = dr["course_name"].ToString(),
                            Source = dr["Source"].ToString(),
                            Price = dr["price_course"].ToString(),
                            TypeCar = dr["type_name"].ToString(),
                        });
                    }
                    conn.Close();
                }
                return Ok(listCar);
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
