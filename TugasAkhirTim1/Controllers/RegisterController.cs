﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;
using Org.BouncyCastle.Crypto.Generators;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Xml.Linq;
using TugasAkhirTim1.Controllers.JwtToken;
using TugasAkhirTim1.Models;
using TugasAkhirTim1.Services.EmailService;

namespace TugasAkhirTim1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        public RegisterController(IConfiguration configuration,IEmailService emailService)
        {
            _configuration = configuration;
            _emailService = emailService;

        }

        [HttpPost]
        [Route("RegisterUser")]
        public IActionResult RegisterUser([FromBody] RegisterUserModel User)
        {
            try
            {
                string token = "";
                using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    if (!CheckUserEmail(User.Email))
                    {
                        conn.Close();
                        return Conflict(new { message = $"Email '{User.Email}' was already used.\n Please sign in" });
                    }
                    string query = "INSERT INTO tbl_users VALUES (Default,@name,@email,@password,1,Default,Default); SELECT LAST_INSERT_ID();";
                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@name", User.Name);
                    cmd.Parameters.AddWithValue("@email", User.Email);
                    string HashPassword = BCrypt.Net.BCrypt.HashPassword(User.Password);
                    cmd.Parameters.AddWithValue("@password", HashPassword);
                    var insertedID = cmd.ExecuteScalar();
                    token = GenerateToken(insertedID.ToString());
                    conn.Close();
                }

                // generate token jwt
                 
                EmailModel model = new EmailModel();
                model.EmailTo = User.Email;
                model.Subject = "verify your account";
                model.Body = "<a href=\"http://localhost:3000/confirmsignup?token=[link]\">verify this link</a>".Replace("[link]", token);
                _emailService.SendEmail(model);

                return Ok("User Telah Ditambahkan");


            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        
        private bool CheckUserEmail(string email)
        {
            string query = "SELECT email FROM tbl_users WHERE email=@email";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();

                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@email", email);
                    var getValue = cmd.ExecuteScalar();
                    if (getValue==null)
                    {
                        conn.Close(); 
                        return true;
                    }
                    conn.Close();
                    return false;  

                    
                }

            }catch (Exception ex) { 
            
            return false;
            }
                

        }



        private string GenerateToken(string id)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim> { new Claim("ID", id) };

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);

            string jwt_User = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt_User;

        }
        [HttpGet]
        [Route("ValidateToken")]
        [Authorize]
        public IActionResult ValidateToken()
        {
            try
            {
                var re = Request;
                string Token = re.Headers.Authorization[0].Replace("Bearer ", "");
                var TokenHandler = new JwtSecurityTokenHandler();
                var jwt = TokenHandler.ReadJwtToken(Token);
                string id = "";
                id = jwt.Claims.First(c => string.Equals(c.Type, "ID")).Value;
                using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    string query = "UPDATE tbl_users SET is_active=@is_active WHERE pk_user_id=@id";
                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@is_active", 1);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                    conn.Close();

                }
                return Ok("User Is Active");

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
    


}
