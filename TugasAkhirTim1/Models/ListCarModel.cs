﻿namespace TugasAkhirTim1.Models
{
    public class ListCarModel
    {
        public int? Id { get; set; }
        public string? Name { get; set; }
        public string? Source { get; set; }
        public string? Price { get; set; }  
        public string? TypeCar { get; set; }


    }
}
