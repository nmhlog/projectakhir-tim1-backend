﻿namespace TugasAkhirTim1.Models
{
    public class EmailModel
    {
        public string? EmailTo { get; set; }
        public string? Subject { get; set; }
        public string? Body { get; set; }
    }
}
