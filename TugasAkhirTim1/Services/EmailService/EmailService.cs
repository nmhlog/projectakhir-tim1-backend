﻿using MimeKit;
using TugasAkhirTim1.Models;

namespace TugasAkhirTim1.Services.EmailService
{
    public class EmailService:IEmailService
    {
        private readonly IConfiguration _configuration;
        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void SendEmail(EmailModel EmailSend)
        {
            var email = new MimeMessage();

            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("EmailUserName").Value));
            email.To.Add(MailboxAddress.Parse(EmailSend.EmailTo));
            email.Subject = EmailSend.Subject;
            email.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = EmailSend.Body };

            var smtp = new MailKit.Net.Smtp.SmtpClient();
            smtp.Connect(_configuration.GetSection("EmailHost").Value, Convert.ToInt32(_configuration.GetSection("EmailPort").Value), useSsl: true);
            smtp.Authenticate(_configuration.GetSection("EmailUserName").Value, _configuration.GetSection("EmailPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);

        }
    }
}
