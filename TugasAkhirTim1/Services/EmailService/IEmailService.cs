﻿using TugasAkhirTim1.Models;

namespace TugasAkhirTim1.Services.EmailService
{
    public interface IEmailService
    {
        void SendEmail(EmailModel emailSend);
    }
}
